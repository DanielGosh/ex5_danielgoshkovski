#pragma once

#include "Shape.h"
#include "Point.h"
#include <vector>

class Polygon : virtual public Shape, virtual public Point
{
public:
	Polygon(const std::string& type, const std::string& name);

	// override functions if need (virtual + pure virtual)

protected:
	std::vector<Point> _points;
};