#pragma once
#include "Polygon.h"


namespace myShapes
{
	// Calling it MyRectangle becuase Rectangle is taken by global namespace.
	class Rectangle : public Polygon
	{
	public:
		
		void draw(const Canvas & canvas);
		void clearDraw(const Canvas & canvas);
		// There's a need only for the top left corner 
		Rectangle(const Point& a, double length, double width, const std::string& type, const std::string& name);
		double getArea();
		double getPerimeter();
		void printDetails();
		std::string getType();
		std::string getName();
		virtual ~Rectangle();

		// override functions if need (virtual + pure virtual)

	protected:
		double _length;
		double _width;
		std::string _type;
		std::string _name;
	};
}