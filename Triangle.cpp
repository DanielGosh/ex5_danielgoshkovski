#include "Triangle.h"

Triangle::Triangle(const Point & a, const Point & b, const Point & c, const std::string & type, const std::string & name) : Polygon(type, name)
{
	this->_name = name;
	this->_type = type;
	this->_points.push_back(a);
	this->_points.push_back(b);
	this->_points.push_back(c);
}

double Triangle::getArea()
{
	return 0.0;
}

double Triangle::getPerimeter()
{
	return (this->_points)[0].distance(this->_points[1]) + (this->_points)[1].distance(this->_points[2]) + (this->_points)[2].distance(this->_points[0]);
}

void Triangle::printDetails()
{
	std::cout << "\ntype: " << this->_type << ", name: " << this->_name;
}

std::string Triangle::getType()
{
	return this->_type;
}

std::string Triangle::getName()
{
	return this->_name;
}

void Triangle::draw(const Canvas& canvas)
{
	canvas.draw_triangle(_points[0], _points[1], _points[2]);
}

void Triangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_triangle(_points[0], _points[1], _points[2]);
}