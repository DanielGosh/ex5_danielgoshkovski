#include "Point.h"

Point::Point(double x, double y)
{
	this->_x = x;
	this->_y = y;
}

Point::Point(const Point & other)
{
	this->_x = other.getX();
	this->_y = other.getY();
}

Point::~Point()
{
}

Point Point::operator+(const Point & other) const
{	
	Point a = other;
	a._x = other.getX() + this->_x;
	a._y = other.getY() + this->_y;
	return a;
}

Point & Point::operator+=(const Point & other)
{
	Point a = other;
	a._x = other.getX() + this->_x;
	a._y = other.getY() + this->_y;
	return a;
}

double Point::getX() const
{
	return this->_x;
}

double Point::getY() const
{
	return this->_y;
}

double Point::distance(const Point & other) const
{
	return sqrt(pow(this->_x - other.getX(), 2) + pow(this->_y - other.getY(), 2));
}
