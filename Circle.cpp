#include "Circle.h"

/*couldn't make the constructor..............
Circle::Circle(const Point & center, double radius, const std::string & type, const std::string & name) : Point(center)
{
	this->_radius = radius;
	this->_type = type;
	this->_name = name;
	this->_center = center;
}*/

double Circle::getArea()
{
	return PI * this->_radius * this->_radius;
}

double Circle::getPerimeter()
{
	return PI * this->_radius * 2;
}

void Circle::printDetails()
{
	std::cout << "\ntype: " << this->_type << ", name: " << this->_name;
}


std::string Circle::getType()
{
	return this->_type;
}

std::string Circle::getName()
{
	return this->_name;
}

const Point & Circle::getCenter() const
{
	// TODO: insert return statement here
}

double Circle::getRadius() const
{
	return 0.0;
}

void Circle::draw(const Canvas& canvas)
{
	canvas.draw_circle(getCenter(), getRadius());
}

void Circle::clearDraw(const Canvas& canvas)
{
	canvas.clear_circle(getCenter(), getRadius());
}

