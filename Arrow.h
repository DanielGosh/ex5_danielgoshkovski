#pragma once
#include "Polygon.h"

class Arrow : public Polygon
{
public:
	Arrow(const Point& a, const Point& b, const std::string& type, const std::string& name);
	~Arrow();
	double getArea();
	double getPerimeter();
	void printDetails();
	std::string getType();
	std::string getName();
	virtual void draw(const Canvas& canvas);
	virtual void clearDraw(const Canvas& canvas);

	// override functions if need (virtual + pure virtual)

private:
	std::vector<Point> _points;
	std::string _type;
	std::string _name;
};