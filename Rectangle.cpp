#include "Rectangle.h"

void myShapes::Rectangle::draw(const Canvas& canvas)
{
	canvas.draw_rectangle(_points[0], _points[1]);
}

void myShapes::Rectangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_rectangle(_points[0], _points[1]);
}

myShapes::Rectangle::Rectangle(const Point & a, double length, double width, const std::string & type, const std::string & name) : Polygon(type, name)
{
	this->_points.push_back(a);
	this->_length = length;
	this->_name = name;
	this->_type = type;
	this->_width = width;
}

double myShapes::Rectangle::getArea()
{
	return this->_length * this->_width;
}

double myShapes::Rectangle::getPerimeter()
{
	return (2 * this->_length) + (2 * this->_width);
}

void myShapes::Rectangle::printDetails()
{
	std::cout << "\ntype: " << this->_name << ", name: " << this->_type;
}

std::string myShapes::Rectangle::getType()
{
	return this->_type;
}

std::string myShapes::Rectangle::getName()
{
	return this->_name;
}