#pragma once

#include "Shape.h"
#include "Point.h"

#define PI 3.14

class Circle : virtual public Shape
{
public:
	Circle(const Point& center, double radius, const std::string& type, const std::string& name);
	double getArea();
	double getPerimeter();
	void printDetails();
	~Circle();

	std::string getType();
	std::string getName();
	const Point& getCenter() const;
	double getRadius() const;

	virtual void draw(const Canvas& canvas);
	virtual void clearDraw(const Canvas& canvas);

	// override functions if need (virtual + pure virtual)
protected:
	Point _center;
	double _radius;
	std::string _type;
	std::string _name;
};