#include "Polygon.h"
#include <string>

class Triangle : public Polygon
{
protected:
	std::string _name;
	std::string _type;
public:
	Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name);
	virtual ~Triangle();

	double getArea();
	double getPerimeter();
	void printDetails();
	std::string getType();
	std::string getName();

	void draw(const Canvas & canvas);

	void clearDraw(const Canvas & canvas);

	// override functions if need (virtual + pure virtual)
};