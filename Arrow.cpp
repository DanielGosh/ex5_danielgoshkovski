#include "Arrow.h"

Arrow::Arrow(const Point & a, const Point & b, const std::string & type, const std::string & name) : Polygon(type, name)
{
	this->_type = type;
	this->_name = name;
	this->_points.push_back(a);
	this->_points.push_back(b);
}

double Arrow::getArea()
{
	return 0;
}
double Arrow::getPerimeter()
{
	return (this->_points)[0].distance(this->_points[1]);
}
void Arrow::printDetails()
{
	std::cout << "\ntype: " << this->_type << ", name: " << this->_name;
}
std::string Arrow::getType()
{
	return this->_type;
}
std::string Arrow::getName()
{
	return this->_name;
}
void Arrow::draw(const Canvas& canvas)
{
	canvas.draw_arrow(_points[0], _points[1]);
}
void Arrow::clearDraw(const Canvas& canvas)
{
	canvas.clear_arrow(_points[0], _points[1]);
}
